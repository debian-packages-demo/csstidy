# [csstidy](https://tracker.debian.org/pkg/csstidy)

CSSTidy is an opensource CSS parser and optimiser. https://github.com/Cerdic/CSSTidy

Unofficial howto and demo

## Versions
* C++ (in Debian) and PHP versions
* Status: Inactive http://csstidy.sourceforge.net/
* Orphaned: https://tracker.debian.org/pkg/csstidy
* Not yet in Alpine https://pkgs.alpinelinux.org/packages?name=csstidy